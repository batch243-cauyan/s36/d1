
const express = require('express');
const mongoose = require('mongoose');

// Create an application using express function
const app = express();
const port = 3001;

// This allows us to use all the routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute");
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Allow all the task routes created in taskRoute.js file to use "/tasks" route
app.use("/tasks", taskRoute); //by importing the taskroute the "/tasks" endpoint was imported and the second parameter taskRoute is its origin.

// Database connection
const myURI = "mongodb+srv://admin:admin@zuittbatch243-cauyan.qpz0r65.mongodb.net/B243-to-do?retryWrites=true&w=majority"

mongoose.connect(myURI, {
    useNewUrlParser: true,
    useUnifiedTopology:true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console,"connection error"));
db.once("open", ()=> console.log("We're connected to the cloud database."));




app.listen(port, ()=>{console.log(`Server running on port ${port}`)});